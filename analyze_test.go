package main

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCreateScanJobs(t *testing.T) {
	base, err := filepath.Abs("./qa/fixtures")
	require.NoError(t, err)

	tests := []struct {
		Name         string
		ProjectRoot  string
		ExpectedJobs []scanJob
	}{
		{
			Name:        "when an Android Studio project is scanned",
			ProjectRoot: filepath.Join(base, "java-android"),
			ExpectedJobs: []scanJob{
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "java-android/app"),
				},
			},
		},
		{
			Name:        "when an Eclipse project is scanned",
			ProjectRoot: filepath.Join(base, "java-android-eclipse"),
			ExpectedJobs: []scanJob{
				{
					Kind:           scanKindManifestAndroidEclipse,
					EntrypointPath: filepath.Join(base, "java-android-eclipse"),
				},
			},
		},
		{
			Name:        "when an XCode project is scanned",
			ProjectRoot: filepath.Join(base, "swift-ios"),
			ExpectedJobs: []scanJob{
				{
					Kind:           scanKindManifestIOS,
					EntrypointPath: filepath.Join(base, "swift-ios"),
				},
			},
		},
		{
			Name:        "when a project with multiple Android .apks is scanned",
			ProjectRoot: filepath.Join(base, "java-android-apk"),
			ExpectedJobs: []scanJob{
				{
					Kind:           scanKindBinaryAndroid,
					EntrypointPath: filepath.Join(base, "java-android-apk/android.apk"),
				},
				{
					Kind:           scanKindBinaryAndroid,
					EntrypointPath: filepath.Join(base, "java-android-apk/nested/android.apk"),
				},
			},
		},
		{
			Name:        "when a multimodule Android Studio project is scanned",
			ProjectRoot: filepath.Join(base, "java-android-multimodule"),
			ExpectedJobs: []scanJob{
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "java-android-multimodule/anotherModule"),
				},
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "java-android-multimodule/app"),
				},
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "java-android-multimodule/heavily/nested/module/hello"),
				},
			},
		},
		{
			Name:        "when a repo containing many different projects is scanned",
			ProjectRoot: filepath.Join(base, "kitchen-sink"),
			ExpectedJobs: []scanJob{

				{
					Kind:           scanKindBinaryAndroid,
					EntrypointPath: filepath.Join(base, "kitchen-sink/java-android-apk/android.apk"),
				},
				{
					Kind:           scanKindBinaryAndroid,
					EntrypointPath: filepath.Join(base, "kitchen-sink/java-android-apk/nested/android.apk"),
				},
				{
					Kind:           scanKindManifestIOS,
					EntrypointPath: filepath.Join(base, "kitchen-sink/swift-ios"),
				},
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "kitchen-sink/java-android/app"),
				},
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "kitchen-sink/java-android-multimodule/anotherModule"),
				},
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "kitchen-sink/java-android-multimodule/app"),
				},
				{
					Kind:           scanKindManifestAndroidStudio,
					EntrypointPath: filepath.Join(base, "kitchen-sink/java-android-multimodule/heavily/nested/module/hello"),
				},
			},
		},
		{
			Name:        "when an Android Studio project is scanned, with only an empty <manifest /> element in AndroidManifest.xml",
			ProjectRoot: filepath.Join(base, "java-android-bug-423542"),
			ExpectedJobs: []scanJob{
				{
					Kind:           scanKindManifestAndroidEclipse,
					EntrypointPath: filepath.Join(base, "java-android-bug-423542"),
				},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.Name, func(t *testing.T) {
			actualScanJobs := createScanJobs(tc.ProjectRoot)

			assert.EqualValues(t, tc.ExpectedJobs, actualScanJobs)
		})
	}
}
